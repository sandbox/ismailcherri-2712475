<?php

/**
 * @file
 * Heliwxware admin settings
 */

/**
 * Form builder function for module settings.
 */
function helixware_connection_settings_form() {
  
  $form['helixware_app_key'] = array(
    '#type' => 'textfield', 
    '#title' => t('Helixware App Key'), 
    '#default_value' => variable_get('helixware_app_key', ''), 
    '#size' => 120, 
    '#maxlength' => 256, 
    '#required' => TRUE,
  );
  
  $form['helixware_app_secret'] = array(
    '#type' => 'textfield', 
    '#title' => t('Helixware App Secret'), 
    '#default_value' => variable_get('helixware_app_secret', ''), 
    '#size' => 120, 
    '#maxlength' => 256, 
    '#required' => TRUE,
  );
  
  $form['helixware_server_url'] = array(
    '#type' => 'textfield', 
    '#title' => t('Helixware Server URL'), 
    '#default_value' => variable_get('helixware_server_url', ''), 
    '#size' => 120, 
    '#maxlength' => 256, 
    '#required' => TRUE,
  );

  
  return system_settings_form($form);
}


/**
 * Form validation handler for helixware_connection_settings_form().
 */
function helixware_connection_settings_form_validate(&$form, &$form_state) {
  if (!valid_url($form_state['values']['helixware_server_url'])){
    form_set_error('helixeare_server_url', 'Please enter a valid URL');
  }
}