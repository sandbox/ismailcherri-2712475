<?php
/**
 * This file provides functions to call and interact with the remote HelixWare Server.
 */

/**
 * Call the remote HelixServer API, signing the request with the Application Key and Secret configured in Drupal.
 *
 *
 * @param string $endpoint The endpoint (will be appended to the server URL, must start with a trailing slash).
 * @param string $method The HTTP method.
 * @param string $body The request payload.
 * @param string $content_type The request body content type (default: 'application/json; charset=UTF-8').
 * @param string $accept The accept header content type (default: 'application/json; charset=UTF-8').
 *
 * @return string The response.
 */
function helixware_server_request(
	$endpoint, 
  $method = 'GET', 
  $body = '', 
  $content_type = 'application/json; charset=UTF-8', 
  $accept = 'application/json; charset=UTF-8') {
  
  // Ensure translations don't break during installation.
  $t = get_t();
  
	// Get the configuration settings and die if not set.
	$app_key    = variable_get('helixware_app_key');
	$app_secret = variable_get('helixware_app_secret');
	$server_url = variable_get('helixware_server_url');

	if ( FALSE === $app_key || FALSE === $app_secret || FALSE === $server_url  ) {
		return $t('The plugin is not configured.');
	}

	// Set the full URL.
	$url = $server_url . $endpoint;
  $cert = dirname( __FILE__ ) . '/helixware.pem';
  $context = stream_context_create(
    array(
      'ssl' => array(
        'local_cert' => $cert, 
        'verify_peer' => true, 
        'verify_depth' => 5, 
        'allow_self_signed' => true,
        'disable_compression' => true
        )
    )
  );
  
	// Prepare the default arguments.
	$args = array(
		'method'  => $method,
		'body'    => ( is_array( $body ) ? json_encode( $body ) : $body ),
		'headers' => array(
			'Content-Type'         => $content_type,
			'Accept'               => $accept,
			'X-Application-Key'    => $app_key,
			'X-Application-Secret' => $app_secret
		),
    'timeout'         => 30,
    'redirection'     => 5,
    'context' => $context
	);

	watchdog('helixware', 'Performing a request [ url :: %url ][ end-point :: %endpoint ]', array('%url' => $url, '%endpoint' => $endpoint), WATCHDOG_INFO);
  
	// Perform the request.
	$response = drupal_http_request( $url, $args );
  if(isset($response->error)){
    watchdog('helixware', 'An error occured while calling the remote server. Code: %error_code. Message: $error_message', array('%error_code' => $response->code, '%error_message' => $response->error), WATCHDOG_ERROR);
    return $response->error;
  }
  
	// Return the response as a string.
	return $response;

}

/**
 * Call the remote HelixServer API, signing the request with the Application Key and Secret configured in Drupal.
 *
 *
 * @param string $endpoint The endpoint (will be appended to the server URL, must start with a trailing slash).
 * @param string $content_type The request body content type (default: 'application/json; charset=UTF-8').
 * @param string $accept The accept header content type (default: 'application/json; charset=UTF-8').
 *
 * @return string The response body.
 */
function helixware_server_call(
	$endpoint, 
  $content_type = 'application/json; charset=UTF-8', 
  $accept = 'application/json; charset=UTF-8') {

	$response = helixware_server_request( $endpoint, 'GET', '', $content_type, $accept );

	// Return the response as a string.
	return isset($response->data) ? $response->data : '';
}
